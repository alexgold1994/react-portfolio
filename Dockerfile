FROM node:14.17.0-alpine AS builder
# Add a work directory
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
# Cache and Install dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm ci --silent
# Copy app files
COPY . ./
# Build the app
RUN npm run build

# Bundle static assets with nginx
FROM nginx:1.21.0-alpine as production
# Copy built assets from builder
COPY --from=builder /app/build /usr/share/nginx/html
# Expose port
EXPOSE 80
# Start nginx
CMD ["nginx", "-g", "daemon off;"]